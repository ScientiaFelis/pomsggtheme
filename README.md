# PoMSggtheme

A `ggplot2()` theme for the Pollinator Monitoring Scheme in Sweden.


## Installation

Install with the remotes package. First install remotes it you do not have it:

`install.package('remotes')`

With that you can install the `PoMSggtheme` with: 

`remotes::install_gitlab('ScientiaFelis/pomsggtheme')`



